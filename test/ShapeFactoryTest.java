import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static java.lang.Math.PI;


public class ShapeFactoryTest {

    private ShapeFactory shapeFactory = new ShapeFactory();

    @Test
    public void shouldCircleGetArea() {
        // instead of doing a calulation of the expected
        // area, you might want to consider
        // putting the actual area here to 
        // increase readability of the code

        // Here I have one question, the expected area is a complex double type when it involves with PI.
        // So that is the reason I use a calulation here.
        double radius = 5.0;
        Shape circle = shapeFactory.getShape("CIRCLE", radius);
        assertEquals(PI * radius * radius, circle.area());
    }

    @Test
    public void shouldCircleGetParameter() {
        // instead of doing a calulation of the expected
        // area, you might want to consider
        // putting the actual area here to 
        // increase readability of the code

        // Same here.
        // Here I have one question, the expected area is a complex double type when it involves with PI.
        // So that is the reason I use a calulation here.

        double radius = 5.0;

        Shape circle = shapeFactory.getShape("CIRCLE", radius);
        assertEquals(2 * PI * radius, circle.perimeter());
    }

    @Test
    public void shouldSquareGetArea() {
        double edge = 2.0;

        Shape square = shapeFactory.getShape("SQUARE", edge);
        assertEquals(4.0, square.area());
    }

    @Test
    public void shouldSquareGetPerimeter() {
        double edge = 2.0;

        Shape square = shapeFactory.getShape("SQUARE", edge);
        assertEquals(8.0, square.perimeter());

    }

    @Test
    public void shouldRectangleGetArea() {
        double width = 2.0;
        double height = 4.0;

        Shape rectangle = shapeFactory.getShape("Rectangle", width, height);
        assertEquals(8.0, rectangle.area());
    }

    @Test
    public void shouldRectangleGetPerimeter() {
        double width = 2.0;
        double height = 4.0;

        Shape rectangle = shapeFactory.getShape("Rectangle", width, height);
        assertEquals(12.0, rectangle.perimeter());
    }

}
