// Good attempt of implementing factory design pattern
// I will provide a sample in the email so that you can remove the excessive switch/if-then-else
// next time when you implement the factory design pattern
public class ShapeFactory {

    // to receive different arguments when go with different shapeTypes.
    public Shape getShape(String shapeType, double ...args) {
        if (shapeType == null) {
            return null;
        }
        if (shapeType.equalsIgnoreCase("CIRCLE")) {
            return new Circle(args[0]);
        } else if (shapeType.equalsIgnoreCase("RECTANGLE")) {
            return new Rectangle(args[0], args[1]);
        } else if (shapeType.equalsIgnoreCase("SQUARE")) {
          // instead of calling Square here, we can call return new Rechtagle(arg[0], arg[0]) 
          // so that you dont need to create square class at all
          // which is one of the objective of this dojo
            return new Square(args[0]);
        } else {
            return null;
        }
    }
}
