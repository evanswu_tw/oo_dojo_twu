// as mentioned in the OO recap Dojo
// the aim of square is not to create a square class
// and not to use inheritance
// think about how you can create a square using a static method i.e. like how factory design pattern works
public class Square implements Shape{

    private final double edge;

    public Square(double edge) {
        this.edge = edge;
    }

    @Override
    public double area() {
        return edge * edge;
    }

    @Override
    public double perimeter() {
        return 4 * edge;
    }
}

