// think about how Rectangle and Circle share similar characteristic
// and what OO concept you can apply to ensure both of these fullfill a similar contract
public class Rectangle implements Shape{

    private final double width;
    private final double height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double area() {
        return width * height;
    }

    @Override
    public double perimeter(){
        return  2 * (height + width);
    }
}
